# social-nature

A [Sails](http://sailsjs.org) application.

# Configuration

The connections configuration is found at `config/connections.js`. Database credentials will need to be set. These values they should be set as environmental variables, but are left as hardcoded values here due to lack of time.

# Notes

This server application is a very basic REST API fro the Users resource. The database used in development was MySQL.
